Boot the board and choose your USB drive as the boot device, connect an ethernet cable to the port closest to the serial console port

After booting the USB drive choose the configure keyboard option

Then, the option "Detect network hardware"

After the detection choose "Configure network". Select enp1s0 as your primary network interface during installation.

If your system is connected to a VLAN trunk port answer 'yes' if not, answer 'no'.

Select 'yes' on the "Auto configuration" prompt, this will allow a faster configuration for the installation. Set the waiting time as you desire. After that the auto-configuration of the interface will be done.

Give a hostname and a domain name to your liking.

After that you will return to the "main menu". Select the "Choose a mirror of the Ubuntu archive".
Select 'http' protocol and then your country. Then the mirror available in that country. And after that, type an HTTP proxy if you need one, or press enter for 'none'.

In the "main menu" select "Download installer components". If you need any additional components select them with space bar. Press Enter when you are done.

After the loading is completed. Select "Set up users and passwords" from the "main menu".

Prompt: "Enable Shadow passwords?" Answer: 'no'

Prompt: "Allow login as root?" Answer: 'no' (this will allow you to elevate to root permissions from an SSH connection)

Create your main user.

Prompt: "Encrypt your home directory?" Answer: 'no'

Select "Configure the clock". Select "english" as language. Select your location. Then the language you desire.

Prompt: "Set the clock using NTP?" Answer: 'yes'
Use ubuntu NTP server. Check if the timezone is correct and say 'yes'.

Select "Detect disks" from the main menu. (you should have and SDcard, or an HardDrive connected to the system)

Select "Partition disks" from the main menu.
Select "Guided: use entire disk" then select your storage device.
Select "All files in one partition"
Set a swap partition with the size you desire, and a primary partition with a Linux file system (ex: ext4), set the mount point to "/" and set the "bootable" flag to 'on'.
Then select "Finish partitioning and write changes to disk".

Select "Install the base system" from the main menu.

In the kernel list, find the "linux-image-generic-lts-xenial" and select it.
On selecting the drivers, choose "Targeted: only include drivers needed for this system".

After the base installation select "Configure the package manager" from the main menu.

Prompt: "Use restricted software?" Answer: 'no'
Prompt: "Use software from the 'universe' component?" Answer: 'yes' (iptables-persistent is from universe component)
Prompt: "Use software from the 'multiverse' component?" Answer: 'no'
Prompt: "Use backported software?" Answer: 'no'
Prompt: "Enable source repositories in APT?" Answer: 'yes'

Select "Select and install software" from the main menu.

Prompt: "How do you want to manage upgrades on this system?" Answer: answer as you desire.

From the list of software you want to install, find and use space bar to select "OpenSSH server", then press Enter.


Select "Install the GRUB boot loader on a hard disk" from the main menu.

Prompt: "Install the GRUB boot loader to the master boot record" Answer: 'yes'

Select your storage device.
Select 'yes' on the next prompt.

Select "Finish the installation" from the main menu.

Prompt: "Is the system clock set to UTC?" Answer: 'yes'

Select 'Continue', wait for the board to reboot, remove the USB drive, the Ubuntu is now installed.

After the installation, SSH into the board and follow the instructions in the help forum to enable serial console access: https://help.ubuntu.com/community/SerialConsoleHowto