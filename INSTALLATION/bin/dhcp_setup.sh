 
valid_dhcp_conf(){

	TEMP_POOL_START="$(sed -e 's/\./ /g' <<< $DHCP_POOL_START)" 
	read -a TEMP_POOL_START_ARRAY <<< $TEMP_POOL_START

	TEMP_POOL_END="$(sed -e 's/\./ /g' <<< $DHCP_POOL_END)" 
	read -a TEMP_POOL_END_ARRAY <<< $TEMP_POOL_END

	TEMP_LAN_NETWORK="$(sed -e 's/\./ /g' <<< $LAN_NETWORK)"
	read -a TEMP_LAN_NETWORK_ARRAY <<< $TEMP_LAN_NETWORK

	if [ ${TEMP_POOL_START_ARRAY[2]} -eq ${TEMP_LAN_NETWORK_ARRAY[2]} ] && [ ${TEMP_POOL_END_ARRAY[2]} -eq ${TEMP_LAN_NETWORK_ARRAY[2]} ]; then
		if [ ${TEMP_POOL_START_ARRAY[3]} -gt "1" ] && [ ${TEMP_POOL_END_ARRAY[3]} -lt "255" ] && [ ${TEMP_POOL_START_ARRAY[3]} -lt ${TEMP_POOL_END_ARRAY[3]} ]; then
			return 0
		else
			echo "ERROR: LAN DHCP pool out of bounds!!!" >> $LOG_FILE
			return 1
		fi
	else
		echo "ERROR: LAN DHCP pool in the wrong network!!!" >> $LOG_FILE
		return 1
	fi
}

valid_wlan_dhcp_conf(){

	TEMP_POOL_START="$(sed -e 's/\./ /g' <<< $WLAN_DHCP_POOL_START)" 
	read -a TEMP_POOL_START_ARRAY <<< $TEMP_POOL_START

	TEMP_POOL_END="$(sed -e 's/\./ /g' <<< $WLAN_DHCP_POOL_END)" 
	read -a TEMP_POOL_END_ARRAY <<< $TEMP_POOL_END

	TEMP_WLAN_NETWORK="$(sed -e 's/\./ /g' <<< $WIRELESS_NETWORK)"
	read -a TEMP_WLAN_NETWORK_ARRAY <<< $TEMP_WLAN_NETWORK

	if [ ${TEMP_POOL_START_ARRAY[2]} -eq ${TEMP_WLAN_NETWORK_ARRAY[2]} ] && [ ${TEMP_POOL_END_ARRAY[2]} -eq ${TEMP_WLAN_NETWORK_ARRAY[2]} ]; then
		if [ ${TEMP_POOL_START_ARRAY[3]} -gt "1" ] && [ ${TEMP_POOL_END_ARRAY[3]} -lt "255" ] && [ ${TEMP_POOL_START_ARRAY[3]} -lt ${TEMP_POOL_END_ARRAY[3]} ]; then
			return 0
		else
			echo "ERROR: WLAN DHCP pool out of bounds!!!" >> $LOG_FILE
			return 1
		fi
	else
		echo "ERROR: WLAN DHCP pool in the wrong network!!!" >> $LOG_FILE
		return 1
	fi
}

valid_other_lan_dhcp_conf(){

	TEMP_POOL_START="$(sed -e 's/\./ /g' <<< $OTHER_LAN_DHCP_POOL_START)" 
	read -a TEMP_POOL_START_ARRAY <<< $TEMP_POOL_START

	TEMP_POOL_END="$(sed -e 's/\./ /g' <<< $OTHER_LAN_DHCP_POOL_END)" 
	read -a TEMP_POOL_END_ARRAY <<< $TEMP_POOL_END

	TEMP_OTHER_LAN_NETWORK="$(sed -e 's/\./ /g' <<< $OTHER_LAN_DHCP_NETWORK)"
	read -a TEMP_OTHER_LAN_NETWORK_ARRAY <<< $TEMP_OTHER_LAN_NETWORK

	if [ ${TEMP_POOL_START_ARRAY[2]} -eq ${TEMP_OTHER_LAN_NETWORK_ARRAY[2]} ] && [ ${TEMP_POOL_END_ARRAY[2]} -eq ${TEMP_OTHER_LAN_NETWORK_ARRAY[2]} ]; then
		if [ ${TEMP_POOL_START_ARRAY[3]} -gt "1" ] && [ ${TEMP_POOL_END_ARRAY[3]} -lt "255" ] && [ ${TEMP_POOL_START_ARRAY[3]} -lt ${TEMP_POOL_END_ARRAY[3]} ]; then
			return 0
		else
			echo "ERROR: OTHER_LAN DHCP pool out of bounds!!!" >> $LOG_FILE
			return 1
		fi
	else
		echo "ERROR: OTHER_LAN DHCP pool in the wrong network!!!" >> $LOG_FILE
		return 1
	fi
}


valid_lan3_dhcp_conf(){

	TEMP_POOL_START="$(sed -e 's/\./ /g' <<< $LAN3_DHCP_POOL_START)" 
	read -a TEMP_POOL_START_ARRAY <<< $TEMP_POOL_START

	TEMP_POOL_END="$(sed -e 's/\./ /g' <<< $LAN3_DHCP_POOL_END)" 
	read -a TEMP_POOL_END_ARRAY <<< $TEMP_POOL_END

	TEMP_LAN3_NETWORK="$(sed -e 's/\./ /g' <<< $LAN3_DHCP_NETWORK)"
	read -a TEMP_LAN3_NETWORK_ARRAY <<< $TEMP_LAN3_NETWORK

	if [ ${TEMP_POOL_START_ARRAY[2]} -eq ${TEMP_LAN3_NETWORK_ARRAY[2]} ] && [ ${TEMP_POOL_END_ARRAY[2]} -eq ${TEMP_OTHER_LAN_NETWORK_ARRAY[2]} ]; then
		if [ ${TEMP_POOL_START_ARRAY[3]} -gt "1" ] && [ ${TEMP_POOL_END_ARRAY[3]} -lt "255" ] && [ ${TEMP_POOL_START_ARRAY[3]} -lt ${TEMP_POOL_END_ARRAY[3]} ]; then
			return 0
		else
			echo "ERROR: LAN3 DHCP pool out of bounds!!!" >> $LOG_FILE
			return 1
		fi
	else
		echo "ERROR: LAN3 DHCP pool in the wrong network!!!" >> $LOG_FILE
		return 1
	fi
}


dhcp_listen_iface_setup(){

	echo "INTERFACES=\"$DHCP_IFACES\""
}
