#!/bin/bash

# $1 is the version we want to get
KERNEL_V=$1
apt-get remove -y linux-headers-generic linux-image-generic linux-image-generic-lts-xenial linux-headers-generic-lts-xenial

apt-get install -y linux-image-$KERNEL_V-generic linux-headers-$KERNEL_V linux-headers-$KERNEL_V-generic linux-image-extra-$KERNEL_V-generic

apt-get remove -y linux-headers-4.4.0-119 linux-headers-4.4.0-119-generic linux-image-4.4.0-119-generic linux-image-extra-4.4.0-119-generic

update-grub
