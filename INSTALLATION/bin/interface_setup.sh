. ${INTERFACE_AUX} --source-only


iface_setup() {

re='^[0-9]+$'

echo "auto lo
iface lo inet loopback" 

	##### LAN SETUP #####
	echo_bridge_iface $LAN_IFACE $LAN_STATIC_IP $LAN_NETWORK $LAN_NETMASK $LAN_BROADCAST $LAN_BRIDGE_PORT $TMP_DNS_SERVER

	##### WAN SETUP #####
	if [ "$WAN_METHOD" = "dhcp" ]
		then
			if [ "$WAN_VLAN_ID" = "" ]
				then
					echo_dhcp_iface $WAN_IFACE
			elif ! [[ "$WAN_VLAN_ID" =~ $re ]]
				then
					echo "ERROR: bad config file!!! Location: WAN vlan id\n" >> $LOG_FILE
					exit 1
			else
				echo_dhcp_iface_vlan $WAN_IFACE $WAN_VLAN_ID
			fi

	elif [ "$WAN_METHOD" = "static" ]
		then
			if [ "$WAN_VLAN_ID" = "" ]
				then
					echo_wan_static_iface $WAN_IFACE $WAN_STATIC_IP $WAN_NETWORK $WAN_NETMASK $WAN_BROADCAST $WAN_GATEWAY
			elif ! [[ "$WAN_VLAN_ID" =~ $re ]]
				then
					echo "ERROR: bad config file!!! Location: WAN vlan id\n" >> $LOG_FILE
					exit 1
			else
				echo_wan_static_iface_vlan $WAN_IFACE $WAN_STATIC_IP $WAN_NETMASK $WAN_VLAN_ID $WAN_BROADCAST $WAN_GATEWAY
			fi

	elif [ "$WAN_METHOD" = "none" ]
		then
			echo "No WAN interface config!\n" >> $LOG_FILE
	else
		echo "ERROR: bad config file!!! Location: WAN interface.\n" >> $LOG_FILE
		exit 1
	fi

	##### WAN BKP SETUP #####

	if [ "$WAN_BKP_METHOD" = "dhcp" ]
		then
			if [ "$WAN_BKP_VLAN_ID" = "" ]
				then
					echo_dhcp_iface $WAN_BKP_IFACE
			elif ! [[ "$WAN_BKP_VLAN_ID" =~ $re ]]
				then
					echo "ERROR: bad config file!!! Location: WAN_BKP vlan id.\n" >> $LOG_FILE
			else
				echo_dhcp_iface_vlan $WAN_BKP_IFACE $WAN_BKP_VLAN_ID
			fi

	elif [ "$WAN_BKP_METHOD" = "static" ]
		then
			if [ "$WAN_BKP_VLAN_ID" = "" ]
				then
					echo_wan_static_iface $WAN_BKP_IFACE $WAN_BKP_STATIC_IP $WAN_BKP_NETWORK $WAN_BKP_NETMASK $WAN_BKP_BROADCAST $WAN_BKP_GATEWAY
			elif ! [[ "$WAN_BKP_VLAN_ID" =~ $re ]]
				then
					echo "ERROR: bad config file!!! Location: WAN_BKP vlan id.\n" >> $LOG_FILE
			else
				echo_wan_static_iface_vlan $WAN_BKP_IFACE $WAN_BKP_STATIC_IP $WAN_BKP_NETMASK $WAN_BKP_VLAN_ID $WAN_BKP_BROADCAST $WAN_BKP_GATEWAY
			fi

	elif [ "$WAN_BKP_METHOD" = "none" ]
		then
			echo "No WAN_BKP interface config! \n" >> $LOG_FILE
	else
		echo "ERROR: bad config file!!! Location: WAN_BKP interface.\n" >> $LOG_FILE
	fi

	##### WIRELESS SETUP #####
	echo_static_iface $WIRELESS_IFACE $WIRELESS_STATIC_IP $WIRELESS_NETWORK $WIRELESS_NETMASK $WIRELESS_BROADCAST


	##### MGMT SETUP #####
	if [[ $LAN_AUX_METHOD = "static" ]]; then
		echo_static_iface $LAN_AUX_IFACE $LAN_AUX_STATIC_IP $LAN_AUX_NETWORK $LAN_AUX_NETMASK $LAN_AUX_BROADCAST

	else
		echo "No LAN_AUX interface config! \n" >> $LOG_FILE
	fi

	##### LAN3 SETUP #####
	if [[ $LAN3_METHOD = "static" ]]; then
		echo_static_iface $LAN3_IFACE $LAN3_STATIC_IP $LAN3_NETWORK $LAN3_NETMASK $LAN3_BROADCAST

	else
		echo "No LAN3 interface config! \n" >> $LOG_FILE
	fi
}